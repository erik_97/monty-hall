use rand::seq::{IteratorRandom, SliceRandom};
use rand::{thread_rng, Rng};

const RUNS: u32 = 1_000;

fn main() -> Result<(), ()> {
    let mut rng = thread_rng();

    let mut win = 0u32;
    for _ in 0..RUNS {
        let mut game = Game::new();
        let random_door = rng.gen_range(0, 3);
        game.select_door(random_door)?;
        if game.open_door()? {
            win += 1;
        }
    }

    let win_ratio = win as f64 / RUNS as f64;
    println!("Win ratio: {} (didn't switch doors)", win_ratio);

    let mut win = 0u32;
    for _ in 0..RUNS {
        let mut game = Game::new();
        let random_door = rng.gen_range(0, 3);
        game.select_door(random_door)?;
        game.switch_door()?;
        if game.open_door()? {
            win += 1;
        }
    }

    let win_ratio = win as f64 / RUNS as f64;
    println!("Win ratio: {} (switched doors)", win_ratio);

    Ok(())
}

#[derive(Debug)]
struct Game {
    doors: Vec<Prize>,
    selected: Option<usize>,
}

impl Game {
    /// Create a new game with three doors, with no door selected  
    /// Two doors have a goat behind them  
    /// One door has a car behind it  
    fn new() -> Game {
        let mut doors = Vec::<Prize>::new();
        doors.push(Prize::Car);
        doors.push(Prize::Goat);
        doors.push(Prize::Goat);

        let mut rng = thread_rng();
        doors.shuffle(&mut rng);

        Game {
            doors: doors,
            selected: None,
        }
    }

    /// Select on of the three doors  
    /// Returns an error when no valid door is selected  
    fn select_door(&mut self, door_number: usize) -> Result<(), ()> {
        if door_number > 2 {
            return Err(());
        }

        self.selected = Some(door_number);

        Ok(())
    }

    /// Returns a result containing true if the car was behind the selected door, false otherwise  
    /// Returns an error when no valid door is selected  
    /// Ends the game by consuming it  
    fn open_door(self) -> Result<bool, ()> {
        let selected = self.selected.ok_or(())?;
        Ok(self.doors[selected] == Prize::Car)
    }

    /// This functions open one of the remaining doors with a goat and switched the selected door to the remaining closed door  
    /// Returns an error when no valid door is selected  
    fn switch_door(&mut self) -> Result<(), ()> {
        let mut rng = rand::thread_rng();

        let selected = self.selected.ok_or(())?;

        let skip_door = (0..=2)
            .filter(|&x| x != selected && self.doors[x] == Prize::Goat)
            .choose(&mut rng)
            .ok_or(())?;

        let new_selected = (0..=2).filter(|&x| x != selected && x != skip_door).next();

        match new_selected {
            None => Err(()),
            Some(_) => {
                self.selected = new_selected;
                Ok(())
            }
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum Prize {
    Goat,
    Car,
}
